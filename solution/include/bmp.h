#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "file.h"
#include "image.h"
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>

struct bmp_header create_bmp_header(const struct image* image);

enum read_status_code from_bmp(FILE* in, struct image* image);

enum write_status_code to_bmp(FILE* out, struct image const* image);

#endif //IMAGE_TRANSFORMER_BMP_H
