#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <malloc.h>
#include <stdint.h>

struct image {
    uint64_t height, width;
    struct pixel* pixels;
};

struct pixel {
    uint8_t b, g, r;
};

struct pixel pixel_get(struct image image, uint64_t row, uint64_t column);

void pixel_set(struct image image, struct pixel pixel, uint64_t row, uint64_t column);

struct image create_image(uint64_t width, uint64_t height);

void destroy_image(struct image image);

struct image rotate_image(struct image source_image);

#endif //IMAGE_TRANSFORMER_IMAGE_H
