#ifndef IMAGE_TRANSFORMER_FILE_H
#define IMAGE_TRANSFORMER_FILE_H

#include <image.h>
#include <stdbool.h>
#include <stdio.h>

enum open_status_code {
    OPEN_OK = 0,
    OPEN_ERROR,
};


enum read_status_code {
    READ_OK = 0,
    READ_SIGNATURE_ERROR,
    READ_BITS_ERROR,
    READ_HEADER_ERROR,
    READ_PATH_ERROR
};

enum write_status_code {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum close_status_code {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

void print_open_status_code(enum open_status_code status);
void print_read_status_code(enum read_status_code status);
void print_write_status_code(enum write_status_code status);

bool open_file(FILE** file, const char* filename, const char* mode);
bool close_file(FILE* file);


#endif //IMAGE_TRANSFORMER_FILE_H
