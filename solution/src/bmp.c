#include "bmp.h"
#include "file.h"

#define FILE_TYPE 0x4D42
#define RESERVED_BYTES 0
#define HEADER_SIZE 40
#define PLANES 1
#define BITS_PER_PIXEL 24
#define COMPRESSION 0
#define PIXELS_PER_METER_X 0
#define PIXELS_PER_METER_Y 0
#define COLORS_USED 0
#define COLORS_IMPORTANT 0

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

uint8_t get_padding(uint64_t width){
    return (4 - width * sizeof(struct pixel) % 4) % 4;
}

struct bmp_header create_bmp_header(const struct image* image){
    return (struct bmp_header){
            .bfType = FILE_TYPE,
            .bfileSize = sizeof(struct bmp_header) + (image->width * sizeof(struct pixel) + get_padding(image->width)) * image->height,
            .bfReserved = RESERVED_BYTES,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = HEADER_SIZE,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = PLANES,
            .biBitCount = BITS_PER_PIXEL,
            .biCompression = COMPRESSION,
            .biSizeImage = image->width * sizeof(struct pixel) + get_padding(image->width) * image->height,
            .biXPelsPerMeter = PIXELS_PER_METER_X,
            .biYPelsPerMeter = PIXELS_PER_METER_Y,
            .biClrUsed = COLORS_USED,
            .biClrImportant = COLORS_IMPORTANT
    };
}


enum read_status_code from_bmp(FILE* in, struct image* image){ //поменять мб

    struct bmp_header bmp_header = {0};

    if (fread(&bmp_header, sizeof(bmp_header), 1, in) != 1)
        return READ_HEADER_ERROR;
    if (bmp_header.bfType != FILE_TYPE)
        return READ_SIGNATURE_ERROR;
    if (bmp_header.biBitCount != BITS_PER_PIXEL)
        return READ_BITS_ERROR;
    if (bmp_header.biSize != HEADER_SIZE)
        return READ_HEADER_ERROR;

    *image = create_image(bmp_header.biWidth, bmp_header.biHeight);

    if (image->pixels != NULL) {
        const uint8_t padding = get_padding(bmp_header.biWidth);

        for (uint64_t i = 0; i < image->height; i++) {
            if (fread(image->pixels + image->width * i, sizeof(struct pixel), image->width, in) != image->width) {
                return READ_HEADER_ERROR;
            }
            if (fseek(in, padding, SEEK_CUR)) {
                return READ_HEADER_ERROR;
            }
        }
    }
    return READ_OK;
}

enum write_status_code to_bmp(FILE* out, struct image const* image) {
    struct bmp_header bmp_header = create_bmp_header(image);
    const uint8_t padding = get_padding(image->width);

    if(fwrite(&bmp_header, sizeof(bmp_header), 1, out) != 1){
        return WRITE_ERROR;
    }

    for (uint64_t i = 0; i < image->height; i++) {
        if (fwrite(image->pixels + image->width * i, sizeof(struct pixel), image->width,out) != image->width) {
            return WRITE_ERROR;
        }
        if (fwrite(image->pixels, 1, padding, out) != padding)
            return WRITE_ERROR;
    }
    return WRITE_OK;
}
