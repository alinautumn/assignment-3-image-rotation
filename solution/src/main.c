#include <file.h>
#include <image.h>
#include <stdio.h>

#include "bmp.h"

#define ARGUMENT_NUM 3

int main( int argc, char** argv ) {
    if (argc != ARGUMENT_NUM) {
        fprintf(stderr, "Неверное количество аргументов");
        return 0;
    }

    FILE *input_file;
    FILE *output_file;

    enum open_status_code open_input_file = open_file(&input_file, argv[1], "rb");
    enum open_status_code open_output_file = open_file(&output_file, argv[2], "wb");

    if (!open_input_file) {
        if (open_output_file)
            close_file(output_file);
        return 0;
    }

    if (!open_output_file) {
        close_file(input_file);
        return 0;
    }

    struct image img = {0};

    enum read_status_code read_status = from_bmp(input_file, &img);
    if (read_status != READ_OK) {
        print_read_status_code(read_status);
        return 0;
    }
    struct image rotated_image = rotate_image(img);

    if (rotated_image.pixels != NULL) {
        enum write_status_code write_status = to_bmp(output_file, &rotated_image);
        if (write_status != WRITE_OK) {
            print_write_status_code(write_status);
            return 0;
        }
    }

    destroy_image(img);
    destroy_image(rotated_image);

    close_file(input_file);
    close_file(output_file);

    return 0;
}
