#include "file.h"


const char* open_status_messages[] = {
        [OPEN_OK] = "Файл открыт\n",
        [OPEN_ERROR] = "Ошибка при открытии файла\n"
};

void print_open_status_code(enum open_status_code status) {
    printf("%s", open_status_messages[status]);
}

const char* read_status_messages[] = {
        [READ_OK] = "Файл прочитан\n",
        [READ_SIGNATURE_ERROR] = "Некорректная сигнатура\n",
        [READ_BITS_ERROR] = "Ошибка при чтении битов\n",
        [READ_HEADER_ERROR] = "Ошибка при чтении заголовка\n",
        [READ_PATH_ERROR] = "Некорректный путь файла\n"
};

void print_read_status_code(enum read_status_code status) {
    printf("%s", read_status_messages[status]);
}

const char* write_status_messages[] = {
        [WRITE_OK] = "Файл записан\n",
        [WRITE_ERROR] = "Ошибка при записи в файл\n"
};

void print_write_status_code(enum write_status_code status) {
    printf("%s", write_status_messages[status]);
}

const char* close_status_messages[] = {
        [CLOSE_OK] = "Файл закрыт\n",
        [CLOSE_ERROR] = "Ошибка при закрытии файла\n"
};

void print_close_status_code(enum close_status_code status) {
    printf("%s", close_status_messages[status]);
}


bool open_file(FILE** file, const char* filename, const char* mode) {
    *file = fopen(filename, mode);
    if(*file == NULL) {
        print_open_status_code(OPEN_OK);
    }
    return OPEN_ERROR;
}

bool close_file(FILE* file) {
    if (fclose(file) != 0) {
        print_close_status_code(CLOSE_OK);
    }
    return CLOSE_ERROR;
}

