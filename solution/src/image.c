#include "image.h"

struct pixel pixel_get(const struct image image, uint64_t row, uint64_t column) {
    return image.pixels[row * image.width + column];
}

void pixel_set(struct image image, const struct pixel pixel, uint64_t row, uint64_t column) {
    image.pixels[row * image.width + column] = pixel;
}

struct image create_image(uint64_t width, uint64_t height) {
    return (struct image) {
            .height = height,
            .width = width,
            .pixels = malloc(sizeof(struct pixel) * width * height)};
}

void destroy_image(struct image image) {
    if(image.pixels != NULL)
        free(image.pixels);
}

struct image rotate_image(struct image source_image) {
    struct image rotated_image = create_image(source_image.height, source_image.width);

    for (uint64_t row = 0; row < source_image.width; row++) {
        for (uint64_t column = 0; column < source_image.height; column++) {
            pixel_set(rotated_image, pixel_get(source_image, column, row), row, rotated_image.width - 1 - column);
        }
    }
    return rotated_image;
}
